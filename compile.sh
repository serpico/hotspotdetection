#!/bin/sh

cd $(dirname "$0")
mkdir build
cd build
cmake ..
make
cd ..

#install
dir=$(pwd)
mv $dir/build/bin/hotSpotDetection /usr/local/bin/hotSpotDetection
