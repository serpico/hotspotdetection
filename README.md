# Hot Spot Detection

The HotSpotDetection software written in C++ enables to detect fluorescence accumulation over time in video-microscopy. The cumulated detection maps enable to extract more reliably the regions of interest. In practice, this method only requires the setting of the false alarm probability.

We adopt a MRF framework to capture image regularity. In contrast to the usual pixel-wise MRF models, a recent line of work consists in modeling non-local interactions from image patches. The redundancy property and patch-based representation is here exploited to detect unusual spatial patterns seen in the scene. This property holds true in fluorescence imaging and we propose a patch-based Gibbs/MRF modeling to represent the more regular image components. Furthermore, we detect the locations where redundancy is low, that is protein concentrations against a nearly uniform background ideally.

# Install

1. Install TBI from this project: https://gitlab.inria.fr/serpico/trackingbinaryinterface
2. cd hotspotdetection
3. mkdir build
4. cmake ..
5. make 


# Reference 
Th. Pécot, Ch. Kervrann, S. Bardin, B. Goud, J. Salamero. **Patch-based Markov models for event detection in fluorescence bioimaging.** In Int. Conf. on Medical Image Computing and Computer Assisted Intervention (MICCAI'08), Volume 2, Pages 95-103, New York City, USA, September 2008 (see http://www.irisa.fr/vista/Papers/2009_miccai_pecot.pdf
