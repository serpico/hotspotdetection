#include <hotspotdetection>
#include "hotspotdetectionTestConfig.h"

#include <iostream>

int main(int argc, char *argv[])
{

    std::cout << "input image = " << SAMPLE << std::endl;
    std::cout << "result image = " << RESULT << std::endl;

    cimg_library::CImg<double> inputImage(SAMPLE);
    CImg<unsigned short> outputImage = hotspotdetection_function( inputImage, 3, 5, 0.2 );


    // calculate error with the reference image
    cimg_library::CImg<unsigned short> resultImage(RESULT);
    double error = resultImage.MSE(outputImage);
    if (error > 10)
    {
        return 1;
    }
    return 0;

}
