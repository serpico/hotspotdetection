#include <hotspotdetection>

#include <iostream>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <vector>
#include "math.h"

int main(int argc,char** argv){

	const char * image_entree  = cimg_option("-i",(char*)NULL,"input image sequence");
	const char * output_file  = cimg_option("-o",(char*)NULL,"output file");
	const int motif = cimg_option("-m",3,"patch size");
	const int neighborhoodSize = cimg_option("-n",5,"neighborhood size");
	const double pValue = cimg_option("-pv",.2,"p-value for false alarm");

	if(image_entree!=NULL){
		CImg<double> Im_entree(image_entree);
        CImg<unsigned short> affiche = hotspotdetection_function( Im_entree, motif, neighborhoodSize, pValue );

		if(output_file){
			affiche.save(output_file);
		}
		else{
			affiche.save("output.tif");
		}
		cout << endl;
	}


	return(0);

}
