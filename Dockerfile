FROM ubuntu:20.04

WORKDIR /app

COPY . /app

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install cmake && \
    apt-get -y install g++ && \
    apt-get -y install libtiff-dev && \
    apt-get -y install libpng-dev && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make

ENV PATH="/app/build/bin:$PATH"

CMD ["bash"]
