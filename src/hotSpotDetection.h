#pragma once

#include "CImg.h"

using namespace cimg_library;
using namespace std;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////            fonctions utilisees dans le main         /////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//calcule la fonction d'energie
CImg<double> energieArecouvrement(int n,int m,int motif,int pas_motif,CImg<double> & support_travail,double sigma,double beta,double mu,double & maxR,int vois,CImg<double> & normalisation);


/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : compare
_Subject  : Compare two integers
-----------------------------------------------------------------------------*/

int compare(const void *i, const void *j);

/*===========================================================================*/




/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : SigmaLTS
_Subject  : Compute the robust standard deviation of the data defined by
           Rousseeuw and Leroy on the complete image support (Least Trimmed
           of Squares)
-----------------------------------------------------------------------------*/

float SigmaLTS(CImg<double> tab,int N);

/*===========================================================================*/



/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : PseudoResidu
_Subject  :

-----------------------------------------------------------------------------*/

void  PseudoResidu(CImg<double> pixm, CImg<double> & residu, int w, int h);

/* Main function */
CImg<unsigned short> hotspotdetection_function( CImg<double> &Im_entree, int motif, int neighborhoodSize, double pValue );

