
#include "hotSpotDetection.h"

#include <iostream>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <vector>
#include "math.h"

#define max(x,y) x>y?x:y
using namespace cimg_library;
using namespace std;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////            fonctions utilisees dans le main         /////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//calcule la fonction d'energie
CImg<double> energieArecouvrement(int n,int m,int motif,int pas_motif,CImg<double> & support_travail,double sigma,double beta,double mu,double & maxR,int vois,CImg<double> & normalisation)
		{
	CImg<double> energie(n,m,1,1,0);
	for(int y=vois+pas_motif;y<(m-vois-pas_motif);y++){
		for(int x=vois+pas_motif;x<(n-vois-pas_motif);x++){
			double distance = 0;
			for(int i=-vois;i<=vois;i++){
				for(int j=-vois;j<=vois;j++){
					if((i!=0)||(j!=0)){
						double dispersion=0;
						for(int u=-pas_motif;u<=pas_motif;u++){
							for(int v=-pas_motif;v<=pas_motif;v++){
								dispersion += pow(support_travail(x+j+v,y+i+u)-support_travail(x+v,y+u),2);
							}
						}
						distance += dispersion;
					}
				}
			}
			double norm = 1;
			normalisation(x,y) = norm;
			if((distance/(2*pow(sigma,2)))>0.0001){
				energie(x,y) = (beta*max((distance/(4*pow(sigma,2))) - ((motif*motif)/2-1)*log(distance/(2*pow(sigma,2)))-mu,0))/norm;
			}
			if(energie(x,y)>maxR){
				maxR = energie(x,y);
			}
		}
	}

	return energie;
		}


/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : compare
_Subject  : Compare two integers
-----------------------------------------------------------------------------*/

int compare(const void *i, const void *j)
{
	float a=*((float *)i);
	float b=*((float *)j);

	if (a > b)
		return (1);
	else
		if (a < b)
			return (-1);
		else
			return (0);
}

/*===========================================================================*/




/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : SigmaLTS
_Subject  : Compute the robust standard deviation of the data defined by
           Rousseeuw and Leroy on the complete image support (Least Trimmed
           of Squares)
-----------------------------------------------------------------------------*/

float SigmaLTS(CImg<double> tab,int N)
{
	int    i;
	int    k=0;
	int    Nmed;
	float  sigma;
	float  *I;


	/* Initialization */
	/* -------------- */
	I = new float [N];

	for(int y=0;y<tab.height();y++){
		for(int x=0;x<tab.width();x++){
			if(tab(x,y)!=0){
				I[k] = pow(tab(x,y),2);
				k++;
			}
		}
	}

	N = k - 1;

	if (N<=0)
		sigma = 0.1;
	else
	{
		/* Computation of Med_i(tab) */
		/* ------------------------- */
		qsort((float *) I, N, sizeof(float), compare);
		sigma = 0;
		Nmed  = (int)(0.5*N);

		for (i=0; i<Nmed; i++)
			sigma += I[i];

		sigma = max(0.1, 2.6477*sqrt(sigma/Nmed));

	}

	delete[] I;

	return(sigma);
}

/*===========================================================================*/



/*=============================================================================

-------------------------------------------------------------------------------

_Function_name  : PseudoResidu
_Subject  :

-----------------------------------------------------------------------------*/

void  PseudoResidu(CImg<double> pixm, CImg<double> & residu, int w, int h)
{
	int i, j;

	for(i=0; i<w-1; i++)
		for(j=0; j<h-1; j++){
			residu(i,j)=(2*pixm(i,j)-pixm(i+1,j)-pixm(i,j+1))/sqrt(6);
		}


	i=w-1;
	for(j=0; j<h-1; j++){
		residu(i,j)=(2*(pixm(i,j))-pixm(i-1,j)-pixm(i,j+1))/sqrt(6);
	}

	j=h-1;
	for(i=0; i<w-1; i++){
		residu(i,j)=(2*(pixm(i,j))-pixm(i+1,j)-pixm(i,j-1))/sqrt(6);
	}

	i=w-1;j=h-1;
	residu(i,j)=(2*(pixm(i,j))-pixm(i-1,j)-pixm(i,j-1))/sqrt(6);


}

CImg<unsigned short> hotspotdetection_function( CImg<double> &Im_entree, int motif, int neighborhoodSize, double pValue ){

    CImg<unsigned short> affiche(Im_entree.width(),Im_entree.height(),1,1,0);

    //initialisation du pas de decalage pour recentrer les motifs (distance entre le point central et les extremites du motif)
    int pas_motif=(motif-1)/2;


    for(int t=0;t<Im_entree.depth();t++){
        cout << "\rComputation [" << t+1 << '/' << Im_entree.depth() << ']';fflush(stdout);

        CImg<double> image(Im_entree.width(),Im_entree.height(),1,1,0);
        for(int y=0;y<Im_entree.height();y++){
            for(int x=0;x<Im_entree.width();x++){
                image(x,y) = Im_entree(x,y,t);
            }
        }
        CImg<double> residuG(image.width(),image.height(),1,1,0);
        PseudoResidu(image,residuG,image.width(),image.height());
        double sig = SigmaLTS(residuG,image.width()*image.height());

        //calcul de la fonction d'energie
        double beta=1,mu=0,maxR=0;
        CImg<double> norm(image.width(),image.height(),1,1,0);
        CImg<double> resultatInter = energieArecouvrement(image.width(),image.height(),motif,pas_motif,image,sig,beta,mu,maxR,neighborhoodSize,norm);

        int valMin=1;
        vector<double> paretoD;
        for(int y=0;y<image.height();y++){
            for(int x=0;x<image.width();x++){
                //resultat(x,y,t) = resultatInter(x,y);
                //normalisation(x,y,t) = norm(x,y);
                if(resultatInter(x,y)>valMin){
                    paretoD.push_back(resultatInter(x,y));
                }
            }
        }
        double sommeP=0;
        for(unsigned int u=0;u<paretoD.size();u++){
            sommeP += log(paretoD[u]);
        }

        double paretoP = paretoD.size()/(sommeP-log(valMin));
        double seuil = exp(log(valMin) - log(pValue)/paretoP);
        for(int y=0;y<image.height();y++){
            for(int x=0;x<image.width();x++){
                if(resultatInter(x,y)>seuil){
                    affiche(x,y) += 1;
                }
            }
        }
    }

    return affiche;
}
